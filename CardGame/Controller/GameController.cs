﻿namespace CardGame
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;

    public class GameController
    {
        private static GameController instance;

        public static GameController Instance => instance ?? (instance = new GameController());

        public Board Board;
        public Grid MainGrid;

        private PlayerLocation playerToStart;
        private int playerTurns;
        private int roundTimer;
        private DispatcherTimer timer;

        public void Awake(Grid mainWindow)
        {
            Board = new Board(mainWindow);
            MainGrid = mainWindow;

            Start();
        }

        // This get's called to start the game
        public void Start()
        {
            for (int i = 0; i < 8; i++)
            {
                foreach (PlayerLocation playerLocation in Enum.GetValues(typeof(PlayerLocation)))
                {
                    Card card = Board.GiveCardToPlayer(playerLocation);
                    if (card.Equals(Helper.SevenOfClubs))
                    {
                        playerToStart = playerLocation;
                    }
                }
            }

            Board.Players.FirstOrDefault(x => x.PlayerLocation == playerToStart).AtPlay = true;

            DoRound();
        }

        public void DoRound()
        {
            Board.DoPlayerTurn();
            playerTurns = 0;

            timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(1000) };
            timer.Tick += TimerOnTick;
            timer.Start();
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            if (playerTurns <= 2)
            {
                Board.ChangePlayerTurn();
                Board.DoPlayerTurn();
                playerTurns++;
            }
            else
            {
                timer.Stop();
                Button button = (Button)MainGrid.FindName("btnProceed");
                if (button != null)
                {
                    button.Click += ButtonOnClick;
                }
            }
        }

        private void ButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            Button button = (Button)sender;
            button.Click -= ButtonOnClick;

            Board.ClearPlayingField();
            DoRound();
        }
    }
}
