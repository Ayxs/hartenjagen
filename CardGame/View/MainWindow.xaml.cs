﻿using System.Windows;

namespace CardGame
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Board Board;

        public MainWindow()
        {
            InitializeComponent();
            btnStart.Click += BtnStartOnClick;
        }

        private void BtnStartOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            btnStart.Visibility = Visibility.Hidden;
            btnStart.Click -= BtnStartOnClick;

            GameController.Instance.Awake(tableTop);
            SetOffsets();
        }

        private void SetOffsets()
        {
            SetOffsetsBetweenCards(EastHand.Children, Orientation.Vertical);
            SetOffsetsBetweenCards(WestHand.Children, Orientation.Vertical);
            SetOffsetsBetweenCards(NorthHand.Children, Orientation.Horizontal);
            SetOffsetsBetweenCards(SouthHand.Children, Orientation.Horizontal);
        }

        private void SetOffsetsBetweenCards(UIElementCollection hand, Orientation orientation)
        {
            int cardCount = hand.Count;
            double middle = (cardCount / 2) - 0.5;

            foreach (Rectangle card in hand)
            {
                int index = hand.IndexOf(card);
                double timesOffset = Math.Abs(index - middle);
                double offset = timesOffset * card.Width;

                if (orientation == Orientation.Horizontal)
                {
                    card.Margin = new Thickness(index > middle ? offset : 0, 0, index < middle ? offset : 0, 0);
                }
                else if (orientation == Orientation.Vertical)
                {
                    card.Margin = new Thickness(0, index > middle ? offset : 0, 0, index < middle ? offset : 0);
                }
            }
        }

        private void BtnRestartClick(object sender, RoutedEventArgs e)
        {
            EastHand.Children.Clear();
            NorthHand.Children.Clear();
            WestHand.Children.Clear();
            SouthHand.Children.Clear();

            PlayerEastPlayingArea.Children.Clear();
            PlayerNorthPlayingArea.Children.Clear();
            PlayerSouthPlayingArea.Children.Clear();
            PlayerWestPlayingArea.Children.Clear();

            GameController.Instance.Awake(tableTop);
            SetOffsets();
        }
    }
}
