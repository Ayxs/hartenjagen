﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CardGame
{
    public class Card
    {
        private CardColor _cardColor;
        private CardValue _cardValue;
        private bool isTurnedAround;

        private static int cardWidth = 178;
        private static int cardHeight = 250;
        private static double cardViewboxWidth = 0.125;
        private static double cardViewboxHeight = 0.25;

        public CardColor CardColor
        {
            get => _cardColor;
            set => _cardColor = value;
        }

        public CardValue CardValue
        {
            get => _cardValue;
            set => _cardValue = value;
        }

        public Card(CardColor cardColor, CardValue cardValue)
        {
            this._cardColor = cardColor;
            this._cardValue = cardValue;
        }

        public Card(CardColor cardColor, CardValue cardValue, bool isTurnedAround)
        {
            _cardColor = cardColor;
            _cardValue = cardValue;
            this.isTurnedAround = isTurnedAround;
        }

        public void FaceUp()
        {
            this.isTurnedAround = false;
        }

        public void FaceDown()
        {
            this.isTurnedAround = true;
        }

        public Rectangle GenerateCardView()
        {
            Rectangle card = new Rectangle();

            if (isTurnedAround)
            {
                card.Width = cardWidth;
                card.Height = cardHeight;

                BitmapImage image = new BitmapImage(new Uri("pack://application:,,,/Resources\\card_back.png"));
                ImageBrush brush = new ImageBrush(image);

                card.Fill = brush;
            }
            else
            {
                double viewboxVLocation = 0.25 * (double)_cardColor;
                double viewboxHLocation = 0.125 * (double)_cardValue;

                // Create a rectangle.
                card.Width = cardWidth;
                card.Height = cardHeight;

                // Load the image.
                BitmapImage theImage =
                    new BitmapImage(
                        new Uri("pack://application:,,,/Resources\\card_graphics.png"));
                ImageBrush brush = new ImageBrush(theImage);

                brush.Viewbox = new Rect(viewboxHLocation, viewboxVLocation, cardViewboxWidth, cardViewboxHeight);
                brush.ViewboxUnits = BrushMappingMode.RelativeToBoundingBox;
                brush.Viewport = new Rect(0, 0, 1, 1);
                brush.ViewportUnits = BrushMappingMode.RelativeToBoundingBox;
                brush.TileMode = TileMode.Tile;
                brush.Stretch = Stretch.Fill;
                brush.AlignmentX = AlignmentX.Center;
                brush.AlignmentY = AlignmentY.Center;


                // Use the ImageBrush to paint the rectangle's background.
                card.Fill = brush;
                card.Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0));
                card.StrokeThickness = 2;
            }

            card.RenderTransformOrigin = new Point(0.5, 0.5);
            card.Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            card.StrokeThickness = 2;

            return card;
        }

        public override string ToString()
        {
            return $"{CardValue.ToString()} of {CardColor.ToString()}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Card);
        }

        public bool Equals(Card card)
        {
            return card != null && card.CardColor == this.CardColor && card.CardValue == CardValue;
        }

        public static bool operator <(Card c1, Card c2)
        {
            return c1.CardValue < c2.CardValue;
        }

        public static bool operator >(Card c1, Card c2)
        {
            return c1.CardValue > c2.CardValue;
        }
    }
}
