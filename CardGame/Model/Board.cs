﻿namespace CardGame
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Shapes;

    public class Board
    {
        public CurrentRound CurrentRound;
        public List<Player> Players = new List<Player>();
        public List<Card> Deck = new List<Card>();

        public Card CardToMatchThisRound;
        public Dictionary<PlayerLocation, Card> CardsPlayedThisRound = new Dictionary<PlayerLocation, Card>();

        private Grid grid;
        private Player player;

        private bool firstRound = true;
        private PlayerLocation nextStart;

        public Board(Grid cardGrid)
        {
            grid = cardGrid;
            CurrentRound = CurrentRound.Normal;

            Players.Add(new Player(0, PlayerLocation.West));
            Players.Add(new Player(0, PlayerLocation.North));
            Players.Add(new Player(0, PlayerLocation.East));
            Players.Add(new Player(0, PlayerLocation.South));

            foreach (CardColor color in Enum.GetValues(typeof(CardColor)))
            {
                foreach (CardValue value in Enum.GetValues(typeof(CardValue)))
                {
                    Deck.Add(new Card(color, value));
                }
            }

            Deck.Shuffle();
        }

        public void ClearPlayingField()
        {
            if (firstRound) firstRound = false;
            List<Card> cardlist = CardsPlayedThisRound.Values.ToList();
            nextStart = CardsPlayedThisRound.FirstOrDefault(x => Equals(x.Value, cardlist.GetWinningCard(CardToMatchThisRound))).Key;
            CardToMatchThisRound = null;
            CardsPlayedThisRound.Clear();

            foreach (Player pl in Players)
            {
                Grid playingArea = (Grid)grid.FindName($"Player{pl.PlayerLocation.ToString()}PlayingArea");
                playingArea?.Children.Clear();
                pl.AtPlay = false;

                if (pl.PlayerLocation == nextStart)
                {
                    pl.AtPlay = true;
                }
            }
        }

        public void ChangePlayerTurn()
        {
            int nextPlayerIndex = 0;

            foreach (Player player in Players)
            {
                if (player.AtPlay)
                {
                    int currentPlayerIndex = Players.IndexOf(player);
                    nextPlayerIndex = currentPlayerIndex + 1;

                    if (nextPlayerIndex == 4)
                    {
                        nextPlayerIndex = 0;
                    }
                }

                player.AtPlay = false;
            }

            Players[nextPlayerIndex].AtPlay = true;
        }

        public Card DoPlayerTurn()
        {
            player = Players.FirstOrDefault(x => x.AtPlay);

            if (player != null && CurrentRound == CurrentRound.Normal && CardToMatchThisRound == null)
            {
                Card toPlay = player.Hand.GetLowestCard();
                if(player.PlayerLocation != PlayerLocation.South) toPlay.FaceUp();
                Rectangle rect = toPlay.GenerateCardView();

                Grid playingArea = (Grid)grid.FindName($"Player{player.PlayerLocation.ToString()}PlayingArea");
                playingArea?.Children.Add(rect);

                RemoveCardFromPlayer(player.Hand.IndexOf(toPlay), player.PlayerLocation);
                player.Hand.Remove(toPlay);

                CardToMatchThisRound = toPlay;
                CardsPlayedThisRound.Add(player.PlayerLocation, toPlay);

                Debug.WriteLine(player.PlayerLocation + " played a " + toPlay);

                return toPlay;
            }

            if (player != null && CurrentRound == CurrentRound.Normal)
            {
                Card toPlay = player.Hand.GetCardToPlay(CurrentRound, CardToMatchThisRound);
                if (player.PlayerLocation != PlayerLocation.South) toPlay.FaceUp();
                Rectangle rect = toPlay.GenerateCardView();

                Grid playingArea = (Grid)grid.FindName($"Player{player.PlayerLocation.ToString()}PlayingArea");
                playingArea?.Children.Add(rect);

                RemoveCardFromPlayer(player.Hand.IndexOf(toPlay), player.PlayerLocation);
                player.Hand.Remove(toPlay);

                CardsPlayedThisRound.Add(player.PlayerLocation, toPlay);

                Debug.WriteLine(player.PlayerLocation + " played a " + toPlay);

                return toPlay;
            }

            return null;
        }

        public Card GiveCardToPlayer(PlayerLocation player)
        {
            Player playerToGiveCard = Players.FirstOrDefault(x => x.PlayerLocation == player);

            if (playerToGiveCard != null && playerToGiveCard.Hand.Count <= 8)
            {
                Card card = GetCardFromDeck();
                playerToGiveCard.Hand.Add(card);

                if (player != PlayerLocation.South)
                {
                    card.FaceDown();
                }

                Rectangle cardView = card.GenerateCardView();

                if (player == PlayerLocation.East || player == PlayerLocation.West)
                {
                    cardView.RenderTransform = new RotateTransform(90);
                }

                Grid playerHand = (Grid)grid.FindName($"{player.ToString()}Hand");
                playerHand?.Children.Add(cardView);

                Debug.WriteLine($"Gave {player.ToString()} a {card}");

                return card;
            }

            return null;
        }

        public void RemoveCardFromPlayer(int cardIndex, PlayerLocation player)
        {
            Grid playerHand = (Grid)grid.FindName($"{player.ToString()}Hand");
            playerHand?.Children.RemoveAt(cardIndex);
        }

        private Card GetCardFromDeck()
        {
            Card card = Deck[0];
            Deck.RemoveAt(0);

            return card;
        }
    }
}
