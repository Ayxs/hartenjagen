﻿namespace CardGame
{
    using System.Collections.Generic;

    public class Player
    {
        public int Score;
        public List<Card> Hand = new List<Card>();
        public PlayerLocation PlayerLocation;
        public bool AtPlay;

        public Player(int score, PlayerLocation playerLocation)
        {
            Score = score;
            PlayerLocation = playerLocation;
        }
    }
}
