﻿namespace CardGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Threading;

    public static class Helper
    {
        public static Card SevenOfClubs = new Card(CardColor.Clubs, CardValue.Seven);
        public static Card JackOfClubs = new Card(CardColor.Clubs, CardValue.Jack);
        public static Card QueenOfSpades = new Card(CardColor.Spades, CardValue.Queen);

        private static Random rng = new Random();

        public static void DelayAction(int millisecond, Action action)
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += delegate

                {
                    action.Invoke();
                    timer.Stop();
                };

            timer.Interval = TimeSpan.FromMilliseconds(millisecond);
            timer.Start();
        }

        /// <summary>
        /// Shuffle a collection using the 'Fisher-Yates' algorithm
        /// </summary>
        /// <typeparam name="T">Type of the items in the list</typeparam>
        /// <param name="list">The collection to be shuffled</param>
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static Card GetLowestCard(this List<Card> cards)
        {
            if (cards != null && cards.Count > 0)
            {
                if (cards.Count > 1)
                {
                    Card lowest = null;
                    foreach (Card card in cards)
                    {
                        if (card.Equals(new Card(CardColor.Clubs, CardValue.Seven)))
                        {
                            return card;
                        }

                        if (lowest == null)
                        {
                            lowest = card < cards[1] ? card : cards[1];
                        }
                        else
                        {
                            if (card < lowest)
                            {
                                lowest = card;
                            }
                        }
                    }

                    return lowest;
                }

                return cards[0];
            }

            return null;
        }

        public static Card GetHighestCard(this List<Card> cards)
        {
            if (cards != null && cards.Count > 0)
            {
                if (cards.Count > 1)
                {
                    Card highset = null;
                    foreach (Card card in cards)
                    {
                        if (highset == null)
                        {
                            highset = card > cards[1] ? card : cards[1];
                        }
                        else
                        {
                            if (card > highset)
                            {
                                highset = card;
                            }
                        }
                    }

                    return highset;
                }

                return cards[0];
            }

            return null;
        }

        public static Card GetCardToPlay(this List<Card> hand, CurrentRound currentRound, Card cardToMatch)
        {
            Card sameColor = hand.Where(x => x.CardColor == cardToMatch.CardColor).ToList().GetLowestCard();

            if (sameColor == null)
            {
                if (hand.Contains(JackOfClubs))
                {
                    return JackOfClubs;
                }

                if (hand.Contains(QueenOfSpades))
                {
                    return QueenOfSpades;
                }

                return currentRound == CurrentRound.Normal ? hand.GetHighestCard() : hand.GetLowestCard();
            }

            return sameColor;
        }

        public static Card GetWinningCard(this List<Card> hand, Card cardToMatch)
        {
            Card winningCard = cardToMatch;
            foreach (Card card in hand)
            {
                if (card.CardColor == cardToMatch.CardColor && card.CardValue > winningCard.CardValue)
                {
                    winningCard = card;
                }
            }

            return winningCard;
        }
    }
}
