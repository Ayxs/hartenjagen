﻿namespace CardGame
{
    public enum CardColor
    {
        Clubs = 0,
        Hearts,
        Spades,
        Diamonds
    }

    public enum CardValue
    {
        Seven = 0,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,
        Ace
    }

    /// <summary>
    /// The location of the player, the South player is always the human player
    /// </summary>
    public enum PlayerLocation
    {
        North,
        East,
        South,
        West
    }

    public enum CurrentRound
    {
        Normal,
        Reverse
    }

    public enum Orientation
    {
        Vertical,
        Horizontal
    }
}
